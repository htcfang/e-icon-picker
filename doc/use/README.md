# 安装


>~~因为项目使用了element-ui的组件进行二次开发，所以在使用此组件前请安装element-ui组件库。~~
>~~安装方式请参考element-ui官网的相关文档。~~[element-ui官网](https://element.eleme.cn/#/zh-CN/component/installation)。
>
>已经脱离element-ui，不需要再安装element-ui了。

### npm安装

推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。

```bash
npm install e-icon-picker -S
```


### cdn 引入
```
<!-- 引入组件库 -->
https://unpkg.zhimg.com/e-icon-picker/dist/index.js
<!-- 引入样式 -->
https://unpkg.zhimg.com/e-icon-picker/dist/index.css

<!-- font-awesome图标样式 -->
https://unpkg.zhimg.com/font-awesome/css/font-awesome.min.css

<!-- element-ui图标样式 -->
https://unpkg.com/element-ui/lib/theme-chalk/icon.css

<!-- 默认彩色图标样式 -->
https://unpkg.zhimg.com/e-icon-picker/dist/symbol.js

```
